from django.apps import AppConfig


class ColtSteeleProjectsConfig(AppConfig):
    name = 'colt_steele_projects'
